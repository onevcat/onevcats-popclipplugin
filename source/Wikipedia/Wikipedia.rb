require 'rubygems'
require 'detect_language'

input=ENV['POPCLIP_TEXT']

DetectLanguage.configure do |config|
  config.api_key = "2685a07a37ac1cb23751c58044916d4d"
end

lang = DetectLanguage.simple_detect(input)

system("open", "http://#{lang}.wikipedia.org/wiki/#{input}")